package de.biowepen.skygames;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.potion.PotionEffectType;

import de.biowepen.skygames.command.SetlocCommand;
import de.biowepen.skygames.game.GameState;
import de.biowepen.skygames.game.LobbyTimer;
import de.biowepen.skygames.game.LocationManager;
import de.biowepen.skygames.game.RestartTimer;
import de.biowepen.skygames.game.TimerHandler;
import de.biowepen.skygames.listener.EntityDamageListener;
import de.biowepen.skygames.listener.FoodLevelChangeListener;
import de.biowepen.skygames.listener.IngameListeners;
import de.biowepen.skygames.listener.PlayerBuildListener;
import de.biowepen.skygames.listener.PlayerDropItemListener;
import de.biowepen.skygames.listener.PlayerJoinListener;
import de.biowepen.skygames.listener.PlayerPickupItemListener;
import de.biowepen.skygames.listener.PlayerQuitListener;

public class SkyGames extends JavaPlugin {
	
	public static ArrayList<String> livingPlayers = new ArrayList<String>();
	public static ArrayList<String> spectators = new ArrayList<String>();
	public static String prefix = "�7[�eSkyGames�7] �c";
	public static GameState state = GameState.LOBBY;
	public static int maxplayers = 6;
	public static int minplayers = 2;
	public static int currentTimerTime = 10;
	public static int[] broadcastTimes = new int[]{10, 5, 4, 3, 2, 1};
	public static TimerHandler timerHandler;
	public static int graceTime = 20;
	public static HashMap<Location, Material> chests = new HashMap<Location, Material>();
	public static PotionEffectType[] effects = new PotionEffectType[]{PotionEffectType.REGENERATION, PotionEffectType.SPEED, PotionEffectType.INCREASE_DAMAGE, PotionEffectType.FIRE_RESISTANCE, PotionEffectType.HARM};
	public static Material[] opItems = new Material[]{Material.DIAMOND_SWORD, Material.DIAMOND_CHESTPLATE, Material.DIAMOND_LEGGINGS, Material.DIAMOND_BOOTS, Material.DIAMOND_HELMET};
	public static Material[] armor = new Material[]{Material.IRON_CHESTPLATE, Material.IRON_HELMET, Material.IRON_LEGGINGS, Material.IRON_BOOTS, Material.GOLD_CHESTPLATE, Material.GOLD_LEGGINGS, Material.GOLD_HELMET, Material.GOLD_BOOTS};
	public static Material[] weapons = new Material[]{Material.BOW, Material.IRON_SWORD, Material.WOOD_SWORD, Material.STONE_AXE};
	public static Material[] items = new Material[]{Material.BREAD, Material.APPLE, Material.ARROW, Material.COOKED_BEEF};
	private static HashMap<String, Object> values = new HashMap<String, Object>();
	
	public void onEnable()
	{
		LocationManager.getLocation("GameSpawn").getWorld().setAutoSave(false);
		timerHandler = new TimerHandler(getInstance());
		SkyGames.setValue("LobbyTimerState", "waiting for players");
		
		Bukkit.getPluginManager().registerEvents(new PlayerJoinListener(), this);
		Bukkit.getPluginManager().registerEvents(new PlayerQuitListener(), this);
		Bukkit.getPluginManager().registerEvents(new EntityDamageListener(), this);
		Bukkit.getPluginManager().registerEvents(new FoodLevelChangeListener(), this);
		Bukkit.getPluginManager().registerEvents(new PlayerDropItemListener(), this);
		Bukkit.getPluginManager().registerEvents(new PlayerBuildListener(), this);
		Bukkit.getPluginManager().registerEvents(new PlayerPickupItemListener(), this);
		Bukkit.getPluginManager().registerEvents(new IngameListeners(), this);
		
		getCommand("setloc").setExecutor(new SetlocCommand());
		
		timerHandler.registerTimer("LobbyTimer", new LobbyTimer(), 20);
	}
	
	public void onDisable()
	{
		for(Location loc : chests.keySet())
		{
			loc.getBlock().setType(chests.get(loc));
		}
		chests.clear();
	}
	
	public static void setValue(String key, Object value)
	{
		values.put(key, value);
	}
	
	public static void removeValue(String key)
	{
		if(values.containsKey(key))
		{
			values.remove(key);
		}
	}
	
	public static Object getValue(String key)
	{
		if(values.containsKey(key))
		{
			return values.get(key);
		}
		return null;
	}
	
	public static void handleDeath(Player p)
	{
		p.setHealth(20D);
		SkyGames.livingPlayers.remove(p.getUniqueId().toString());
		SkyGames.spectators.add(p.getUniqueId().toString());
		for(ItemStack items : p.getInventory().getContents())
		{
			if(items != null)
			{
				p.getLocation().getWorld().dropItem(p.getLocation(), items);	
			}
		}
		p.teleport(LocationManager.getLocation("SpectatorSpawn"));
		p.setAllowFlight(true);
		clearInventory(p);
		for(Player players : Bukkit.getOnlinePlayers())
		{
			if(SkyGames.livingPlayers.contains(players.getUniqueId().toString()))
			{
				players.hidePlayer(p);
			}
			if(SkyGames.spectators.contains(players.getUniqueId().toString()))
			{
				players.showPlayer(p);
			}
		}
		if(SkyGames.livingPlayers.size() == 1)
		{
			SkyGames.end(Bukkit.getPlayer(UUID.fromString(SkyGames.livingPlayers.get(0))));
		}
	}
	
	public static void clearInventory(Player p)
	{
		p.getInventory().clear();
		p.getInventory().setHelmet(null);
		p.getInventory().setChestplate(null);
		p.getInventory().setLeggings(null);
		p.getInventory().setBoots(null);
	}
	
	public static void end(Player winner)
	{
		Bukkit.broadcastMessage(SkyGames.prefix + "The Player �6" + winner.getName() + " �cwon the Game.");
		SkyGames.state = GameState.RESTARTING;
		for(Player players : Bukkit.getOnlinePlayers())
		{
			if(SkyGames.livingPlayers.contains(players.getUniqueId().toString()))
			{
				for(Player spectators : Bukkit.getOnlinePlayers())
				{
					if(SkyGames.spectators.contains(spectators.getUniqueId().toString()))
					{
						players.showPlayer(spectators);
					}
				}
			}
			livingPlayers.clear();
			spectators.clear();
			players.teleport(LocationManager.getLocation("Lobby"));
			players.setAllowFlight(false);
		}
		SkyGames.timerHandler.registerTimer("RestartTimer", new RestartTimer(), 20);
	}
	
	public static Plugin getInstance()
	{
		return Bukkit.getPluginManager().getPlugin("SkyGames");
	}
	
}

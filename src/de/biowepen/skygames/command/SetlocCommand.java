package de.biowepen.skygames.command;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import de.biowepen.skygames.SkyGames;
import de.biowepen.skygames.game.LocationManager;

public class SetlocCommand implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		Player p = null;
		if(sender instanceof Player)
		{
			p = (Player)sender;
		}
		if(p != null)
		{
			if(args.length != 1)
			{
				p.sendMessage(SkyGames.prefix + "Available locations: Lobby, GameSpawn, SpectatorSpawn");	
				return true;
			}
			if(args[0].equalsIgnoreCase("Lobby"))
			{
				LocationManager.setLocation("Lobby", p.getLocation());
				p.sendMessage(SkyGames.prefix + "Set Location �6" + args[0] + "�c.");
			} else if(args[0].equalsIgnoreCase("GameSpawn")) {
				LocationManager.setLocation("GameSpawn", p.getLocation());
				p.sendMessage(SkyGames.prefix + "Set Location �6" + args[0] + "�c.");
			} else if(args[0].equalsIgnoreCase("SpectatorSpawn")) {
				LocationManager.setLocation("SpectatorSpawn", p.getLocation());
				p.sendMessage(SkyGames.prefix + "Set Location �6" + args[0] + "�c.");
			} else {
				p.sendMessage(SkyGames.prefix + "Unknown Location");
			}
			return true;
		} else {
			sender.sendMessage("�cYou must be a Player!");
			return true;
		}
	}

}

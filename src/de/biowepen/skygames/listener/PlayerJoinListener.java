package de.biowepen.skygames.listener;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.event.player.PlayerLoginEvent.Result;

import de.biowepen.skygames.SkyGames;
import de.biowepen.skygames.game.GameState;
import de.biowepen.skygames.game.LocationManager;

public class PlayerJoinListener implements Listener {
	
	@EventHandler
	public void onJoin(PlayerJoinEvent e)
	{
		e.getPlayer().setFoodLevel(20);
		SkyGames.clearInventory(e.getPlayer());
		if(SkyGames.state == GameState.LOBBY)
		{
			e.setJoinMessage(SkyGames.prefix + e.getPlayer().getName() + " Joined the Game.");	
			e.getPlayer().teleport(LocationManager.getLocation("Lobby"));
		} else {
			e.setJoinMessage(null);
			e.getPlayer().teleport(LocationManager.getLocation("SpectatorSpawn"));
			SkyGames.spectators.add(e.getPlayer().getUniqueId().toString());
			e.getPlayer().setAllowFlight(true);
			for(Player players : Bukkit.getOnlinePlayers())
			{
				if(SkyGames.livingPlayers.contains(players.getUniqueId().toString()))
				{
					players.hidePlayer(e.getPlayer());
				}
			}
		}
	}
	
	@EventHandler
	public void onLogin(PlayerLoginEvent e)
	{
		if(Bukkit.getOnlinePlayers().size() >= SkyGames.maxplayers && SkyGames.state == GameState.LOBBY)
		{
			e.setKickMessage("�cThis Sever is Full.");
			e.setResult(Result.KICK_FULL);
		}
	}

}

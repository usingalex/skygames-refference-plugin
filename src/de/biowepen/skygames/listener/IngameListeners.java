package de.biowepen.skygames.listener;

import java.util.Random;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;

import de.biowepen.skygames.SkyGames;
import de.biowepen.skygames.game.GameState;

public class IngameListeners implements Listener {
	
	@EventHandler
	public void handleFlightStick(PlayerInteractEvent e)
	{
		if(SkyGames.spectators.contains(e.getPlayer().getUniqueId().toString()))
			return;
		if(SkyGames.state == GameState.INGAME || SkyGames.state == GameState.WARMUP)
		{
			if(e.getAction() == Action.RIGHT_CLICK_AIR || e.getAction() == Action.RIGHT_CLICK_BLOCK)
			{
				if(e.getPlayer().getItemInHand().getType() == Material.BLAZE_ROD)
				{
					Player p = e.getPlayer();
					p.setVelocity(p.getLocation().getDirection().multiply(1D));
					p.setFallDistance(0);
				}
			}	
		}
	}
	
	@EventHandler
	public void handleDeathUnderMinHeight(PlayerMoveEvent e)
	{
		if(e.getPlayer().getLocation().getY() <= 0)
		{
			Bukkit.broadcastMessage(SkyGames.prefix + "�6" + e.getPlayer().getName() + "�c died.");
			SkyGames.handleDeath(e.getPlayer());
		}
	}
	
	@EventHandler
	public void handleChests(BlockBreakEvent e)
	{
		Block b = e.getBlock();
		if(b.getType() == Material.DIAMOND_ORE) //OP CHESTS
		{
			SkyGames.chests.put(b.getLocation(), b.getType());
			e.getBlock().setType(Material.AIR);
			Random rd = new Random();
			int i = rd.nextInt(SkyGames.opItems.length);
			e.getPlayer().getInventory().addItem(new ItemStack(SkyGames.opItems[i], 1));
		}
		if(b.getType() == Material.REDSTONE_ORE || b.getType() == Material.GLOWING_REDSTONE_ORE) //Potion Chest
		{
			SkyGames.chests.put(b.getLocation(), b.getType());
			e.getBlock().setType(Material.AIR);
			Random rd = new Random();
			int i = rd.nextInt(SkyGames.effects.length);
			int d = rd.nextInt(5) + 2;
			int a = rd.nextInt(3) + 1;
			e.getPlayer().addPotionEffect(new PotionEffect(SkyGames.effects[i], d * 20, a));
		}
		if(b.getType() == Material.GOLD_ORE) //ARMOR CHEST
		{
			SkyGames.chests.put(b.getLocation(), b.getType());
			e.getBlock().setType(Material.AIR);
			Random rd = new Random();
			int i = rd.nextInt(SkyGames.armor.length);
			e.getPlayer().getInventory().addItem(new ItemStack(SkyGames.armor[i], 1));
		}
		if(b.getType() == Material.LAPIS_ORE) //Normal CHESTS
		{
			SkyGames.chests.put(b.getLocation(), b.getType());
			e.getBlock().setType(Material.AIR);
			Random rd = new Random();
			int type = rd.nextInt(2);
			int a = rd.nextInt(7) + 1;
			if(type == 0) e.getPlayer().getInventory().addItem(new ItemStack(SkyGames.items[rd.nextInt(SkyGames.items.length)], a));
			if(type == 1) e.getPlayer().getInventory().addItem(new ItemStack(SkyGames.weapons[rd.nextInt(SkyGames.weapons.length)], 1));
		}
	}

}

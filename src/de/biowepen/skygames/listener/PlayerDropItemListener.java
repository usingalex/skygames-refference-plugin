package de.biowepen.skygames.listener;

import org.bukkit.GameMode;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerDropItemEvent;

import de.biowepen.skygames.SkyGames;

public class PlayerDropItemListener implements Listener {
	
	@EventHandler
	public void stopDrop(PlayerDropItemEvent e)
	{
		if(!SkyGames.livingPlayers.contains(e.getPlayer().getUniqueId().toString()))
		{
			if(e.getPlayer().getGameMode() != GameMode.CREATIVE)
				e.setCancelled(true);
		}
	}

}

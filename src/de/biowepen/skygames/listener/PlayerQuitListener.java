package de.biowepen.skygames.listener;

import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

import de.biowepen.skygames.SkyGames;

public class PlayerQuitListener implements Listener {
	
	@EventHandler
	public void onPlayerQuit(PlayerQuitEvent e)
	{
		if(SkyGames.livingPlayers.contains(e.getPlayer().getUniqueId().toString()))
			SkyGames.livingPlayers.remove(e.getPlayer().getUniqueId().toString());
		if(SkyGames.spectators.contains(e.getPlayer().getUniqueId().toString()))
			SkyGames.spectators.remove(e.getPlayer().getUniqueId().toString());
		for(Player players : Bukkit.getOnlinePlayers())
		{
			if(SkyGames.livingPlayers.contains(players.getUniqueId().toString()))
			{
				players.showPlayer(e.getPlayer());
			}
		}
		if(SkyGames.livingPlayers.size() == 1)
		{
			SkyGames.end(Bukkit.getPlayer(UUID.fromString(SkyGames.livingPlayers.get(0))));
		}
	}

}

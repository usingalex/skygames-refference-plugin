package de.biowepen.skygames.listener;

import org.bukkit.GameMode;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;

public class PlayerBuildListener implements Listener {
	
	@EventHandler
	public void stopPlace(BlockPlaceEvent e)
	{
		if(e.getPlayer().getGameMode() != GameMode.CREATIVE)
		{
			e.setCancelled(true);
		}
	}
	
	@EventHandler
	public void stopBreak(BlockBreakEvent e)
	{
		if(e.getPlayer().getGameMode() != GameMode.CREATIVE)
		{
			e.setCancelled(true);
		}
	}
	
}

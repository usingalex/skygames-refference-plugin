package de.biowepen.skygames.listener;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerPickupItemEvent;

import de.biowepen.skygames.SkyGames;

public class PlayerPickupItemListener implements Listener {
	
	@EventHandler
	public void stopSpectatorPickup(PlayerPickupItemEvent e)
	{
		if(SkyGames.spectators.contains(e.getPlayer().getUniqueId().toString()))
			e.setCancelled(true);
	}

}

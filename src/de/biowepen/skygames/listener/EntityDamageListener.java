package de.biowepen.skygames.listener;

import org.bukkit.Bukkit;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;

import de.biowepen.skygames.SkyGames;
import de.biowepen.skygames.game.GameState;

public class EntityDamageListener implements Listener {
	
	@EventHandler
	public void handleDeathByEnviroment(EntityDamageEvent e)
	{
		if(e.getEntity() instanceof Player)
		{
			Player p = (Player) e.getEntity();
			if(e.getDamage() >= p.getHealth())
			{
				if(e.getCause() != DamageCause.ENTITY_ATTACK)
				{
					Bukkit.broadcastMessage(SkyGames.prefix + "�6" + p.getName() + "�c died.");
					e.setCancelled(true);
					SkyGames.handleDeath(p);	
				}
			}
		}
	}
	
	@EventHandler
	public void handleDeathByEntity(EntityDamageByEntityEvent e)
	{
		if(e.getEntity() instanceof Player)
		{
			Player p = (Player) e.getEntity();
			if(SkyGames.spectators.contains(e.getDamager().getUniqueId().toString()))
			{
				e.setCancelled(true);
				return;
			}
			if(e.getDamage() >= p.getHealth())
			{
				if(e.getCause() == DamageCause.ENTITY_ATTACK)
				{
					Entity damager = e.getDamager();
					Bukkit.broadcastMessage(SkyGames.prefix + "�6" + p.getName() + " �cwas killed by �6" + damager.getName() + "�c.");
				}
				e.setCancelled(true);
				SkyGames.handleDeath(p);
			}
		}
	}
	
	@EventHandler
	public void handleDamage(EntityDamageEvent e)
	{
		if(e.getEntity() instanceof Player)
		{
			if(SkyGames.state != GameState.INGAME || SkyGames.spectators.contains(e.getEntity().getUniqueId().toString()))
			{
				e.setCancelled(true);
			}
		}
	}

}

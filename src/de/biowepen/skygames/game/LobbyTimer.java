package de.biowepen.skygames.game;

import org.apache.commons.lang.ArrayUtils;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import de.biowepen.skygames.SkyGames;

public class LobbyTimer implements Runnable {

	@Override
	public void run() {
		if(Bukkit.getOnlinePlayers().size() == SkyGames.minplayers && !SkyGames.getValue("LobbyTimerState").equals("started"))
			SkyGames.setValue("LobbyTimerState", "started");
		if(SkyGames.getValue("LobbyTimerState").equals("started"))
		{
			if(SkyGames.getValue("CurrentTimerTime") == null)
				SkyGames.setValue("CurrentTimerTime", SkyGames.currentTimerTime);
			int time = (int)SkyGames.getValue("CurrentTimerTime");
			if(ArrayUtils.contains(SkyGames.broadcastTimes, time))
			{
				Bukkit.broadcastMessage(SkyGames.prefix + "The Games Starts in �6" + time + " �cSeconds.");
			}
			time--;
			SkyGames.setValue("CurrentTimerTime", time);
			if(time == 0)
			{
				if(Bukkit.getOnlinePlayers().size() >= SkyGames.minplayers)
				{
					for(Player p : Bukkit.getOnlinePlayers())
					{
						SkyGames.livingPlayers.add(p.getUniqueId().toString());
						SkyGames.clearInventory(p);
						p.teleport(LocationManager.getLocation("GameSpawn"));
						ItemStack item = new ItemStack(Material.BLAZE_ROD);
						ItemMeta meta = item.getItemMeta();
						meta.setDisplayName("�cFlight Stick");
						item.setItemMeta(meta);
						p.getInventory().setItem(0, item);
						p.getInventory().setItem(1, new ItemStack(Material.DIAMOND_PICKAXE));
					}
					SkyGames.state = GameState.WARMUP;
					SkyGames.timerHandler.registerTimer("WarmupTimer", new WarmupTimer(), 20);
					SkyGames.setValue("CurrentTimerTime", SkyGames.graceTime);
				} else {
					SkyGames.setValue("LobbyTimerState", "waiting for players");
					SkyGames.setValue("CurrentTimerTime", SkyGames.currentTimerTime);
				}
			}
		}
		if(SkyGames.state != GameState.LOBBY)
		{
			SkyGames.removeValue("LobbyTimerState");
			SkyGames.timerHandler.stopTimer("LobbyTimer");
		}
	}
	
}

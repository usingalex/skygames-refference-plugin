package de.biowepen.skygames.game;

import java.util.HashMap;

import org.bukkit.Bukkit;
import org.bukkit.plugin.Plugin;

public class TimerHandler {
	
	private Plugin plugin;
	private HashMap<String, Integer> timers = new HashMap<String, Integer>();
	
	public TimerHandler(Plugin plugin)
	{
		this.plugin = plugin;
	}
	
	public void registerTimer(String name, Runnable runnable, int interval)
	{
		int task = Bukkit.getScheduler().scheduleSyncRepeatingTask(this.plugin, runnable, 0, interval);
		timers.put(name, task);
	}
	
	public void stopTimer(String name)
	{
		try {
			Bukkit.getScheduler().cancelTask(timers.get(name));	
		} catch(Exception e) {
			e.printStackTrace();
			return;
		}
		timers.remove(name);
	}

}

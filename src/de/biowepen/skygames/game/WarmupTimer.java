package de.biowepen.skygames.game;

import org.apache.commons.lang.ArrayUtils;
import org.bukkit.Bukkit;

import de.biowepen.skygames.SkyGames;

public class WarmupTimer implements Runnable {

	@Override
	public void run() {
		int time = (int)SkyGames.getValue("CurrentTimerTime");
		if(time == SkyGames.graceTime)
		{
			Bukkit.broadcastMessage(SkyGames.prefix + "The Grace Period ends in �6" + time + " �cSeconds.");
		}
		if(ArrayUtils.contains(SkyGames.broadcastTimes, time))
		{
			Bukkit.broadcastMessage(SkyGames.prefix + "The Grace Period ends in �6" + time + " �cSeconds.");
		}
		time--;
		SkyGames.setValue("CurrentTimerTime", time);
		if(time == 0)
		{
			Bukkit.broadcastMessage(SkyGames.prefix + "The Grace Period is Over.");
			SkyGames.state = GameState.INGAME;	
		}
		if(SkyGames.state != GameState.WARMUP)
		{
			SkyGames.timerHandler.stopTimer("WarmupTimer");
			SkyGames.setValue("CurrentTimerTime", 10);
		}
	}
	
}

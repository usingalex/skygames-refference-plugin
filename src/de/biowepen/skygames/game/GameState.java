package de.biowepen.skygames.game;

public enum GameState {
	
	LOBBY,
	WARMUP,
	INGAME,
	RESTARTING;
	
}

package de.biowepen.skygames.game;

import java.io.File;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

public class LocationManager {
	
	public static File file = new File("plugins/locations.yml");
	public static FileConfiguration cfg = YamlConfiguration.loadConfiguration(file);
	
	public static void setLocation(String name, Location loc)
	{
		try {
			cfg.set("locations." + name + ".World", loc.getWorld().getName());
			cfg.set("locations." + name + ".X", loc.getX());
			cfg.set("locations." + name + ".Y", loc.getY());
			cfg.set("locations." + name + ".Z", loc.getZ());
			cfg.set("locations." + name + ".Yaw", loc.getYaw());
			cfg.set("locations." + name + ".Pitch", loc.getPitch());	
			cfg.save(file);
		} catch(Exception e) {
			System.out.println("Can�t save Location '" + name + "'(" + e.getMessage() + ").");
		}
	}
	
	public static Location getLocation(String name)
	{
		try {
			World world = Bukkit.getWorld(cfg.getString("locations." + name + ".World"));
			double X = cfg.getDouble("locations." + name + ".X");
			double Y = cfg.getDouble("locations." + name + ".Y");
			double Z = cfg.getDouble("locations." + name + ".Z");
			double Yaw = cfg.getDouble("locations." + name + ".Yaw");
			double Pitch = cfg.getDouble("locations." + name + ".Pitch");
			return new Location(world, X, Y, Z, (long)Yaw, (long)Pitch);
		} catch(Exception e) {
			System.out.println("Can�t load Location '" + name + "'(" + e.getMessage() + ").");
		}
		return Bukkit.getWorlds().get(0).getSpawnLocation();
	}
	
}

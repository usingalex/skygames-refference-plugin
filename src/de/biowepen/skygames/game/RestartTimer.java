package de.biowepen.skygames.game;

import org.apache.commons.lang.ArrayUtils;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import de.biowepen.skygames.SkyGames;

public class RestartTimer implements Runnable {

	@Override
	public void run() {
		int time = (int)SkyGames.getValue("CurrentTimerTime");
		if(ArrayUtils.contains(SkyGames.broadcastTimes, time))
		{
			Bukkit.broadcastMessage(SkyGames.prefix + "The Server Restarts in �6" + time + " �cSeconds.");
		}
		time--;
		SkyGames.setValue("CurrentTimerTime", time);
		if(time == 0)
		{
			Bukkit.broadcastMessage(SkyGames.prefix + "The Server Restarts now.");
		}
		if(time == -1)
		{
			for(Player players : Bukkit.getOnlinePlayers())
			{
				players.kickPlayer("�cServer Restarting");
			}
			Bukkit.getServer().shutdown();
		}
	}
	
}
